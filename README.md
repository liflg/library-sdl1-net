Website
=======
http://www.libsdl.org/projects/SDL_net/release-1.2.html

License
=======
zlib license (see the file source/COPYING)

Version
=======
1.2.8

Source
======
SDL_net-1.2.8.tar.gz (sha256: 5f4a7a8bb884f793c278ac3f3713be41980c5eedccecff0260411347714facb4)

Requires
========
* SDL1
